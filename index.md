---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

# Curso recuperatorio - Desarrollo de software en entornos libres

Publico los recursos del curso "Desarrollo de software en entornos libres" dado en la Maestría en software libre v1, de la Universidad Andina Simón Bolivia - UASB, en junio de 2018.

## Programa

| Día | Contenido |
| --- | --- |
| 21/06/2018 | [Curso virtual nº1 - Conceptos]({{ site.baseurl }}{% post_url 2018-06-21-Curso-1 %}) |
| 22/06/2018 | [Curso virtual nº2 - Herramientas]({{ site.baseurl }}{% post_url 2018-06-22-Curso-2 %}) |
