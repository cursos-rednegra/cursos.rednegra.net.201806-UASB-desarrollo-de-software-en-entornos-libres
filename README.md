# Instalar

```
git clone --recursive git@framagit.org:severo/cursos.rednegra.net.201806-UASB-desarrollo-de-software-en-entornos-libres.git
```

Si solo has descargado con `git clone` sin la opción `--recursive`, tienes que descargar los submodulos también (reveal.js):

```
git submodule update --init --recursive
```
